    @extends('principal')
    @section('contenido')

    @if(Auth::check())
            @if (Auth::user()->idrol == 1)
            <template v-if="menu==0">
                <dashboard></dashboard>
            </template>

            <template v-if="menu==1">
                <estudiante></estudiante>
            </template>

            <template v-if="menu==2">
                <paralelos></paralelos>
            </template>

            <template v-if="menu==3">
                <promociones></promociones>
            </template>

            <template v-if="menu==4">
                <institutos></institutos>
            </template>

            <template v-if="menu==5">
                <carreras></carreras>
            </template>
            <template v-if="menu==6">
                <estudios></estudios>
            </template>

            <template v-if="menu==7">
                <user></user>
            </template>

            <template v-if="menu==8">
                <rol></rol>
            </template>

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @elseif (Auth::user()->idrol == 2)
            <template v-if="menu==0">
                <dashboard></dashboard>
            </template>

            <template v-if="menu==1">
                <estudiante></estudiante>
            </template>

            <template v-if="menu==2">
                <paralelos></paralelos>
            </template>

            <template v-if="menu==3">
                <promociones></promociones>
            </template>

            <template v-if="menu==4">
                <institutos></institutos>
            </template>

            <template v-if="menu==5">
                <carreras></carreras>
            </template>
            <template v-if="menu==6">
                <estudios></estudios>
            </template>

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @elseif (Auth::user()->idrol == 3)
            <template v-if="menu==0">
                <dashboard></dashboard>
            </template>

            <template v-if="menu==1">
                <estudiante></estudiante>
            </template>

            <template v-if="menu==2">
                <paralelos></paralelos>
            </template>

            <template v-if="menu==3">
                <promociones></promociones>
            </template>

            <template v-if="menu==6">
                <estudios></estudios>
            </template>

           

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @else

            @endif

    @endif
       
        
    @endsection