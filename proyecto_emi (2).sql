-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-08-2021 a las 04:19:25
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_emi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id` int(11) NOT NULL,
  `iduniversidad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nivel` varchar(30) NOT NULL,
  `observacion` varchar(300) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id`, `iduniversidad`, `nombre`, `nivel`, `observacion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, 'FISICA', '', 'FISICA NUCLEAR', 1, '2021-07-27 15:23:29', '2021-07-27 15:23:29'),
(2, 2, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:01:07', '2021-07-27 16:01:07'),
(3, 2, 'MEDICINA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:14:13', '2021-07-27 16:14:13'),
(4, 1, 'BIOLOGIA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:22:31', '2021-07-27 16:22:31'),
(5, 1, 'ENFERMERIA', 'LICENCIATURA', 'PRUEBA', 1, '2021-07-27 16:24:51', '2021-08-11 23:37:51'),
(6, 3, 'TURISMO', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-27 16:40:13', '2021-07-27 16:40:13'),
(7, 3, 'INGENIERIA CIVIL', 'LICENCIATURA', 'INSERT', 1, '2021-07-27 16:40:59', '2021-07-27 16:40:59'),
(8, 1, 'VETERINARIA ZOOTECNIA', 'DOCTORADO', 'NINGUNO AAAAAA', 0, '2021-07-27 16:42:40', '2021-07-27 18:19:47'),
(9, 6, 'ARQUICTECTURA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:27', '2021-07-29 01:15:27'),
(10, 5, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:58', '2021-07-29 01:15:58'),
(11, 5, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:59', '2021-07-29 01:15:59'),
(12, 4, 'MECATRONICA', 'LICENCIATURA', 'LICENCITURA', 1, '2021-08-11 16:21:33', '2021-08-11 16:21:33'),
(13, 4, 'ENFERMERIA', 'TECNICO SUPERIOR', 'DATO DE PRUEBA', 1, '2021-08-11 23:37:26', '2021-08-11 23:37:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institutos`
--

CREATE TABLE `institutos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `sigla` varchar(10) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `institutos`
--

INSERT INTO `institutos` (`id`, `nombre`, `sigla`, `tipo`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 'UNIVERSIDAD MAYOR DE SAN ANDRES', 'UMSA', 'ACREDITADA', 1, 'UNIVERSIDAD ESTATAL PLENA', '2021-07-27 13:52:05', '2021-07-27 13:52:05'),
(2, 'UNIVERSIDAD PUBLICA DE EL ALTO', 'UPEA', 'ESTATAL', 1, 'UNIVERSIDAD PLENA', '2021-07-27 13:41:26', '2021-07-27 15:37:39'),
(3, 'UNIVERSIDAD DE AQUINO BOLIVIA', 'UDABOL', 'PRIVADA', 1, 'PRUEBA', '2021-07-27 16:39:38', '2021-07-27 16:39:38'),
(4, 'UNIVERSIDAD DE LOYOLA', 'LOYOLA', 'PRIVADA', 1, 'ALGO', '2021-07-27 16:42:09', '2021-07-27 16:42:09'),
(5, 'UNIVERSIDAD LA SALLE', 'LA SALLE', 'PRIVADA', 1, 'DATO DE PRUEBA', '2021-07-29 01:14:13', '2021-07-29 01:14:13'),
(6, 'UNIVERSIDAD FRANZ TAMAYO', 'UNIFRANZ', 'PRIVADA', 1, 'DATO DE PRUEBA', '2021-07-29 01:14:15', '2021-07-29 01:14:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_24_153135_create_categorias_table', 1),
(4, '2019_07_31_164422_create_articulos_table', 2),
(5, '2019_08_05_142944_create_personas_table', 3),
(6, '2019_08_05_192617_create_proveedores_table', 4),
(7, '2019_08_05_235406_create_roles_table', 5),
(8, '2019_08_06_000000_create_users_table', 6),
(9, '2019_08_10_100831_create_ingresos_table', 7),
(10, '2019_08_10_101104_create_detalle_ingresos_table', 7),
(11, '2019_08_23_114134_create_ventas_table', 8),
(12, '2019_08_23_114151_create_detalle_ventas_table', 8),
(13, '2019_09_01_192321_create_notifications_table', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('021c8877-23c7-4be9-b5e4-14b1f3de75aa', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:06', '2019-10-22 08:12:06'),
('085d9917-cd57-498e-8bab-a7df2a90a3a1', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2019-09-02 17:38:13', '2019-09-02 17:38:13'),
('0da69d3f-10e6-4363-b9de-e11643c4ba52', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:44', '2019-10-22 08:11:44'),
('10eba0e8-f2ba-443a-846b-356b4de0ddfe', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:50', '2020-12-16 13:15:50'),
('127a078b-8be2-4b6a-9356-601095be88de', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:46', '2020-12-16 13:15:46'),
('14a6c0a8-a9c4-401e-bd09-6bbc9cbc6440', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:06', '2020-01-20 14:14:06'),
('14ba5657-b71b-4c3b-8526-09ce75ac5c38', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', '2019-09-06 07:50:55', '2019-09-02 17:38:13', '2019-09-06 07:50:55'),
('16ba4bdb-563a-4edd-830e-71c9356b3acc', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2019-09-01 18:46:14', '2019-09-01 18:46:14'),
('189c03ba-b2fc-4448-9767-1146d9845269', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:12', '2019-10-22 08:12:12'),
('1911a0f0-ec50-4896-8f38-fcea592459b9', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:56', '2019-10-22 08:11:56'),
('1b33d165-29cb-4304-b342-73449cefb37b', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:38', '2019-10-22 08:11:38'),
('1e3e8814-656e-4238-b471-054be1f310f6', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2019-09-01 18:46:14', '2019-09-01 18:46:14'),
('202c9c67-e1bc-4d05-955b-fd7936da5255', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:13', '2019-10-22 08:11:51', '2019-10-23 14:53:13'),
('206e5518-befd-4d16-8726-0b005233c96a', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:07', '2019-10-22 08:12:07'),
('21852c87-e6d7-417b-9532-80ced2bdc9d6', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2019-09-01 17:38:13', '2019-09-01 17:38:13'),
('25044212-25cf-4b34-a8d4-4f346247edab', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2021-05-28 01:08:50', '2021-04-14 22:31:05', '2021-05-28 01:08:50'),
('2505920e-5ad2-4acf-b9d0-ae5ba4de7d62', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:06', '2021-04-14 22:31:06'),
('2bbfac43-297b-41e4-9d51-c62f7eff2748', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2019-09-02 11:15:16', '2019-09-01 18:46:14', '2019-09-02 11:15:16'),
('2e175250-943d-4ebc-af24-e21d0385cf4a', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:13', '2019-10-22 08:11:54', '2019-10-23 14:53:13'),
('305e01e1-8f13-488b-bd1f-d1bc984c7d9d', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:03', '2020-01-20 14:14:03'),
('34b6cdde-7d6f-4e88-bbf3-ab2ed762de49', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:43', '2019-10-22 08:11:43'),
('35e4c663-1bf9-4c82-a2a4-27ddb7b2ea4b', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:07', '2021-04-14 22:31:07'),
('3c9225d9-334c-4b4b-90b4-142826d01219', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:50', '2020-12-16 13:15:50'),
('3ca97c41-5bc1-430c-9d11-ee6a1e7e881d', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:10', '2019-10-22 08:12:10'),
('3e11ad9d-c259-485a-b1ea-6a196440e0a6', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:06', '2020-01-20 14:14:06'),
('3f2c1434-1c7a-4ab8-8958-e43548baacce', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:14', '2019-10-22 08:11:44', '2019-10-23 14:53:14'),
('4122c8d7-8fd6-4338-b1b4-635e88541fe1', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:07', '2021-04-14 22:31:07'),
('430de0b6-86d5-4351-b1ad-6ec10c30523a', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:05', '2021-04-14 22:31:05'),
('437ac480-2853-4346-9064-c83211846de4', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:46', '2019-10-22 08:11:46'),
('46dd28c8-74ee-4959-86c7-006adb913cda', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:55', '2019-10-22 08:11:55'),
('48b9710b-4b71-40c3-b101-a12944fa6acd', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:10', '2019-10-22 08:12:10'),
('4c6ab2bd-a352-44bb-ad76-ee8fb35c5be4', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:55', '2019-10-22 08:11:55'),
('4ea3ea17-6626-4cc2-8cab-f660f8a95691', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2019-09-02 05:55:55', '2019-09-01 18:46:14', '2019-09-02 05:55:55'),
('50f22d63-b0fe-4c2f-816c-4dd7054144eb', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:54', '2019-10-22 08:11:54'),
('5358e747-2f1b-4ce5-be5f-ee7a561741f9', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2020-02-10 05:47:49', '2020-01-20 14:14:06', '2020-02-10 05:47:49'),
('55128cf8-b48c-4908-88d1-5028569b4827', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:50', '2020-12-16 13:15:50'),
('590c795e-e1b3-4040-934a-979533c5059d', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:07', '2020-01-20 14:14:07'),
('5987642e-1593-4f3c-83fe-a7ba04b4249e', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:54', '2019-10-22 08:11:54'),
('5a5fb6e5-45b3-40df-9317-f0e400c2aa05', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:07', '2019-10-22 08:12:07'),
('5edfbfc4-d135-44b4-8361-7ce6075f66f7', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:12', '2019-10-22 08:12:12'),
('6760a984-9ec7-4d5e-9b80-8b7ebe317f99', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:47', '2020-12-16 13:15:47'),
('6cdd390a-05a0-459c-bf31-f40969558deb', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:07', '2020-01-20 14:14:07'),
('70282ff5-43f0-4dc6-81d0-bcbfb3be5380', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:03', '2019-10-22 08:12:03'),
('71b7a336-3d0c-451e-bd06-070b994c5080', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:58', '2019-10-22 08:11:58'),
('7388d83c-b36d-4824-a3a5-29bb468290fb', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:50', '2020-12-16 13:15:50'),
('7bb8f558-e970-4490-96fe-cfd45c8446cc', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', '2019-09-02 19:21:54', '2019-09-01 17:38:13', '2019-09-02 19:21:54'),
('8556a9ba-1099-41c7-8838-80b802b27cce', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', '2019-09-02 05:49:58', '2019-09-01 17:38:13', '2019-09-02 05:49:58'),
('8792b5f0-246a-40d4-9cd7-73981a41b192', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:50', '2020-12-16 13:15:50'),
('8d58f4a9-3496-4429-ad25-942d8ff38d06', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:13', '2019-10-22 08:12:05', '2019-10-23 14:53:13'),
('8faa896f-c2c6-458c-8bc6-197ebcffea1a', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:47', '2019-10-22 08:11:47'),
('901b083d-d70b-4e24-8a85-3e4dc5383e37', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:12', '2019-10-22 08:12:12'),
('92e72d72-6905-4b05-a0f1-c65777488fc8', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:49', '2019-10-22 08:11:49'),
('9791ed8c-d871-4d0b-be52-88b0f12d116d', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:49', '2020-12-16 13:15:49'),
('9c082ccf-ac02-4fff-8f3a-3bf368ee72cb', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:53', '2019-10-22 08:11:53'),
('9c20ab2d-b66c-48d6-b6ed-93ef415dd1ea', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:44', '2019-10-22 08:11:44'),
('9f25d284-67a2-45fe-95f6-6decaba28f4e', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:48', '2020-12-16 13:15:48'),
('9fb69d53-7f58-42f9-8ad9-e5455611b2b7', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:47', '2019-10-22 08:11:47'),
('a2fdfc28-c5fb-4064-ac2a-f1670b742da5', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:06', '2019-10-22 08:12:06'),
('a47c2283-d273-4c96-bd35-98d607e48c6a', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2019-09-02 19:21:53', '2019-09-01 18:46:14', '2019-09-02 19:21:53'),
('a551af48-2a6d-4f79-95dd-9c3b1d397ced', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2019-09-01 17:38:13', '2019-09-01 17:38:13'),
('a98197a6-0111-4138-960b-daea27ded41e', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:51', '2019-10-22 08:11:51'),
('aafa2af0-3f19-47c4-8dcf-554d81452b8e', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:58', '2019-10-22 08:11:58'),
('b5b02cf6-8746-4b87-a63a-e8648734f5c9', 'App\\Notifications\\NotifyAdmin', 14, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2020-01-20 14:14:06', '2020-01-20 14:14:06'),
('b67ca7a3-8a4b-48ea-b20d-63b5e9b439a6', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:13', '2019-10-22 08:11:58', '2019-10-23 14:53:13'),
('b760f452-4089-468b-8051-9633361a3b30', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:47', '2019-10-22 08:11:47'),
('b94e5b08-991f-4075-acde-9a1e58c6d4f6', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:44', '2020-12-16 13:15:44'),
('b9eadd85-b31d-47e4-99ea-6816f0d393ef', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2021-02-01 19:24:34', '2020-12-16 13:15:50', '2021-02-01 19:24:34'),
('ba3afb6f-c38b-410f-8c63-eac23d6fe577', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:54', '2019-10-22 08:11:54'),
('ba82844f-6adc-460d-9c45-156c1c9cd8bd', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:47', '2019-10-22 08:11:47'),
('c36b0fee-d22d-4778-bd56-b3d4d0039432', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:52', '2019-10-22 08:11:52'),
('c7bb72ae-4780-40e6-aa9f-f953e84e088c', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":6,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:02', '2019-10-22 08:12:02'),
('c9b50f9b-32b2-42ba-9dd9-b694c0a7d752', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:52', '2019-10-22 08:11:52'),
('ce42216c-8502-4311-8e32-4de656aca899', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:54', '2019-10-22 08:11:54'),
('d7726327-da36-404d-8497-1be3bc866d3a', 'App\\Notifications\\NotifyAdmin', 15, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:44', '2019-10-22 08:11:44'),
('d78f3748-e7e2-47eb-83e6-b14934238bd7', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', '2021-02-01 19:24:34', '2020-12-16 13:15:46', '2021-02-01 19:24:34'),
('d7e72ff6-2f95-4f12-b84b-690d7d24d075', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:44', '2019-10-22 08:11:44'),
('d82e5070-1234-4a1a-8934-c0b5271eb733', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:49', '2019-10-22 08:11:49'),
('df36a4a4-f8ad-44e0-bfa4-1132f7a991c0', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:00', '2019-10-22 08:12:00'),
('e2a911b0-7aa3-4ae1-ab7f-8df2119024f2', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:06', '2021-04-14 22:31:06'),
('e4637c32-76b1-4a8e-b3d2-9e522d2bcd48', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":5,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:58', '2019-10-22 08:11:58'),
('e6174252-2f5b-405e-815a-db7d1fc4f971', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2019-09-02 05:49:58', '2019-09-01 18:46:14', '2019-09-02 05:49:58'),
('e80c5aa2-4e07-4e78-8897-2c8c4ae964b3', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:12', '2019-10-22 08:12:11', '2019-10-23 14:53:12'),
('eff7c243-60d0-416f-bc4a-591c829b6df9', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:48', '2019-10-22 08:11:48'),
('f0c2c22a-ce3d-48a5-8db0-bcbac3754d9b', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:46', '2020-12-16 13:15:46'),
('f1d263f8-d996-48e8-a318-3d4e44d7994f', 'App\\Notifications\\NotifyAdmin', 33, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2020-12-16 13:15:47', '2020-12-16 13:15:47'),
('f22b5f15-52d9-4648-8a02-491388ef3298', 'App\\Notifications\\NotifyAdmin', 1, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', '2019-10-23 14:53:13', '2019-10-22 08:11:47', '2019-10-23 14:53:13'),
('f430c130-4ab5-43d5-a89c-7f10355b9b65', 'App\\Notifications\\NotifyAdmin', 19, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":7,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:12:11', '2019-10-22 08:12:11'),
('facaef67-38c8-4371-8d65-e6a09fdb99d5', 'App\\Notifications\\NotifyAdmin', 17, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2021-04-14 22:31:01', '2021-04-14 22:31:01'),
('fd82b0f7-fd97-4655-b1ee-5d8c0f8d3b3b', 'App\\Notifications\\NotifyAdmin', 18, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":0,\"msj\":\"Ingresos\"}}}', NULL, '2019-10-22 08:11:53', '2019-10-22 08:11:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paralelos`
--

CREATE TABLE `paralelos` (
  `id` int(11) NOT NULL,
  `detalle` varchar(45) NOT NULL,
  `condicion` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paralelos`
--

INSERT INTO `paralelos` (`id`, `detalle`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 'ROJO', 1, '2021-06-11 15:11:37', '2021-06-11 14:11:37'),
(2, 'AZUL', 1, '2021-06-11 15:21:32', '2021-06-11 14:21:32'),
(3, 'AMARILLO', 1, '2021-08-17 21:53:01', '2021-08-17 21:53:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_estudios`
--

CREATE TABLE `personal_estudios` (
  `id` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `iduniversidad` int(11) NOT NULL,
  `idcarrera` int(11) NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `nivel` varchar(30) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personal_estudios`
--

INSERT INTO `personal_estudios` (`id`, `idpersonal`, `iduniversidad`, `idcarrera`, `fecha_ingreso`, `nivel`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-08-04', 'LICENCIATURA', 1, 'DATOS DE PRUEBA', '2021-08-10 00:43:18', '2021-08-10 00:36:12'),
(2, 1, 1, 4, '2021-08-11', 'LICENCIATURA', 1, 'DATOS DE PRUEBA', '2021-08-12 00:22:33', '2021-08-12 00:22:33'),
(3, 8, 4, 12, '2020-01-01', 'LICENCIATURA', 1, 'NINGUNA', '2021-08-12 18:06:50', '2021-08-12 18:06:50'),
(4, 8, 6, 9, '2021-01-01', 'TECNICO MEDIO', 1, 'NINGUNA', '2021-08-14 22:49:15', '2021-08-14 22:49:15'),
(5, 2, 1, 4, '2021-01-01', 'DIPLOMADO', 1, 'NINGUNA', '2021-08-14 23:09:40', '2021-08-14 23:09:40'),
(6, 7, 3, 7, '2020-01-01', 'DIPLOMADO', 1, 'NINGUNA', '2021-08-15 01:14:18', '2021-08-15 01:14:18'),
(7, 10, 4, 12, '2016-01-01', 'LICENCIATURA', 1, 'NINGUNA', '2021-08-15 01:22:33', '2021-08-15 01:22:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_promociones`
--

CREATE TABLE `personal_promociones` (
  `id` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idpromocion` int(11) NOT NULL,
  `idparalelo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personal_promociones`
--

INSERT INTO `personal_promociones` (`id`, `idpersona`, `idpromocion`, `idparalelo`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 1, 'NINGUNA', '2021-07-29 00:17:05', '2021-08-17 21:53:28'),
(2, 2, 4, 1, 1, '', '2021-07-29 02:09:07', '2021-07-29 02:09:07'),
(3, 7, 6, 2, 1, 'NINGUNA', '2021-08-08 20:06:48', '2021-08-08 20:06:48'),
(4, 8, 6, 2, 1, 'NINGUNA', '2021-08-08 20:07:02', '2021-08-08 20:07:02'),
(5, 9, 5, 1, 1, 'NINGUNA', '2021-08-08 20:08:52', '2021-08-08 20:08:52'),
(6, 10, 6, 1, 1, 'NINGUNA', '2021-08-11 23:33:28', '2021-08-11 23:33:28'),
(7, 12, 7, 3, 1, 'NINGUNA', '2021-08-18 01:47:36', '2021-08-18 01:47:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(10) NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paterno` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `materno` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_civil` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` enum('MASCULINO','FEMENINO','','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL DEFAULT current_timestamp(),
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `paterno`, `materno`, `estado_civil`, `sexo`, `fecha_nac`, `tipo_documento`, `num_documento`, `direccion`, `telefono`, `email`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 'JOSE', 'QUISPE', 'MORALES', 'SOLTERO', 'MASCULINO', '2020-09-02', 'CI', '6721210', 'LA PAZ EL ALTO', '71238338', 'arming050000@gmail.com', 1, 'NINGUNA', '2021-07-29 19:41:00', '2021-08-17 21:54:13'),
(2, 'YHAMES AMILCAR', 'QUISPE', 'SUCOJAYO', 'SOLTERO', 'MASCULINO', '2014-08-20', 'CI', '13025412', 'LA PAZ', '71238338', 'yamilcar050000@gmail.com', 1, 'NINGUNO', '2021-07-28 23:16:18', '2021-07-28 23:16:18'),
(6, 'JOSE', 'MORALES', 'MORALES', 'CASADO', 'MASCULINO', '1988-01-01', 'CI', '789456', 'LA PAZ', '71238338', 'arminjg08000@gmail.com', 0, 'NINGUNA', '2021-08-08 19:35:46', '2021-08-08 19:35:46'),
(7, 'MARIO MARIO', 'SUAREZ', 'CAMPOS', 'SOLTERO', 'MASCULINO', '1985-02-01', 'CI', '123456', 'EL ALTO', '71238338', 'mario@gmail.com', 1, 'NINGUNA', '2021-08-08 20:06:47', '2021-08-16 02:14:26'),
(8, 'ANA MARIA', 'MAMANI', 'MORALES', 'SOLTERO', 'FEMENINO', '1955-02-01', 'CI', '11111111', 'EL ALTO', '71238338', 'arming050000@gmail.com', 1, 'NINGUNA', '2021-08-08 20:07:02', '2021-08-09 00:01:23'),
(9, 'XIMENA', 'TARQUI', 'FLORES', 'SOLTERO', 'FEMENINO', '1992-01-10', 'CI', '7454121', 'EL ALTO LA PAZ', '73125412', 'xamina@gmail.com', 0, 'NINGUNA', '2021-08-08 20:08:51', '2021-08-09 01:28:20'),
(10, 'MARIA', 'TARQUI', 'FLORES', 'SOLTERO', 'FEMENINO', '2020-02-06', 'CI', '87246231', 'El Alto', '71731240', 'xime717@gmail.com', 1, 'NINGUNA', '2021-08-11 23:33:28', '2021-08-17 21:39:27'),
(11, 'JOSE', 'LOPEZ', 'LOPEZ', 'CASADO', 'MASCULINO', '1988-01-01', 'CI', '741254', 'LA PAZ', '71238338', 'arming050000@gmail.com', 1, 'NINGUNO', '2021-08-15 02:30:56', '2021-08-15 02:30:56'),
(12, 'maria', 'tarqui', 'flores', 'SOLTERO', 'FEMENINO', '2000-02-06', 'CI', '6768904', 'EL ALTO', '754334567', 'ximenatf80@gmail.com', 1, 'NINGUNA', '2021-08-18 01:47:36', '2021-08-18 01:47:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocions`
--

CREATE TABLE `promocions` (
  `id` int(11) NOT NULL,
  `anio` int(4) NOT NULL,
  `descripcion_promo` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `condicion` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `promocions`
--

INSERT INTO `promocions` (`id`, `anio`, `descripcion_promo`, `total`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 2018, 'GARGOLAS', 96, 0, '2021-08-17 22:55:14', '2021-08-17 21:55:14'),
(2, 2004, 'ABRAHAM REYES', 84, 1, '2021-06-11 03:19:45', '2021-06-11 02:19:45'),
(3, 2003, 'FILIAL SANTA CRUZ', 36, 1, '2021-06-11 03:29:13', '2021-06-11 02:29:13'),
(4, 2001, 'FILIAL LA PAZ', 36, 1, '2021-06-11 02:28:38', '2021-06-11 02:28:38'),
(5, 2000, 'FILIAL COCHABAMBA', 153, 1, '2021-06-11 02:29:45', '2021-06-11 02:29:45'),
(6, 1996, 'EPOCA DORADA', 78, 1, '2021-06-11 02:30:09', '2021-06-11 02:30:09'),
(7, 2020, 'EL ALTO', 134, 1, '2021-08-17 21:55:36', '2021-08-17 21:55:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `descripcion`, `condicion`) VALUES
(1, 'Administrador', 'Encargado de la Administracion del Sistema', 1),
(2, 'Director', 'Director de la Unidad Educativa UEFAB', 1),
(3, 'Secretaria', 'Secretaria Area de Reportes Varios', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universidades`
--

CREATE TABLE `universidades` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `sigla` varchar(20) NOT NULL,
  `carreras` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `universidades`
--

INSERT INTO `universidades` (`id`, `descripcion`, `sigla`, `carreras`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'UNIVERSIDAD MAYOR DE SAN ANDRES', 'UMSA', 'INGENIERIA CIVIL', 1, '2021-07-26 02:39:14', '2021-07-26 02:39:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `idrol` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `password`, `condicion`, `idrol`, `remember_token`) VALUES
(11, 'jose', '$2y$10$XpZJYDD6WEMfXMhaHZrW5.y.wkRB6U//vy5qWLwMIGCihi.Coregq', 1, 1, 'iFVhybGP9w0n7zUlJwJpLxDUG3iqGfAOvxGFN2Yng3r3dLb2O3tTZVwRunFW'),
(1, 'ximena', '$2y$10$icHVRo5Dd.rzVGsmMxciQewzdYwfegQHmJ4aM/BYIebv9goi3ekaK', 0, 1, 'xtqp3nlnI4WodjJxaoCuZI2Rez4mMXGk8cLgzzZeco9uREm7p2R3BbrC5sja'),
(2, 'yamilcar', '$2y$10$RUin82GM3UuRODx9r1o6rOtMX5bzd9LTw9BEfHNkeCXRsLazYeqGy', 1, 2, 'pI8krb7HjsHG5szsqxxXKrdc2SSFvIrLUzZ3B76JCmzVehj5kmxJL4HuCTMe');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduniversidad` (`iduniversidad`);

--
-- Indices de la tabla `institutos`
--
ALTER TABLE `institutos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indices de la tabla `paralelos`
--
ALTER TABLE `paralelos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpersonal` (`idpersonal`),
  ADD KEY `iduniversidad` (`iduniversidad`),
  ADD KEY `idcarrera` (`idcarrera`);

--
-- Indices de la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpromocion` (`idpromocion`),
  ADD KEY `idparalelo` (`idparalelo`),
  ADD KEY `idpersona` (`idpersona`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promocions`
--
ALTER TABLE `promocions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_nombre_unique` (`nombre`);

--
-- Indices de la tabla `universidades`
--
ALTER TABLE `universidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_usuario_unique` (`usuario`),
  ADD KEY `users_id_foreign` (`id`),
  ADD KEY `users_idrol_foreign` (`idrol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `institutos`
--
ALTER TABLE `institutos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `paralelos`
--
ALTER TABLE `paralelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `promocions`
--
ALTER TABLE `promocions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `universidades`
--
ALTER TABLE `universidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD CONSTRAINT `carreras_ibfk_1` FOREIGN KEY (`iduniversidad`) REFERENCES `institutos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  ADD CONSTRAINT `personal_estudios_ibfk_1` FOREIGN KEY (`iduniversidad`) REFERENCES `institutos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_estudios_ibfk_2` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_estudios_ibfk_3` FOREIGN KEY (`idpersonal`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  ADD CONSTRAINT `personal_promociones_ibfk_1` FOREIGN KEY (`idparalelo`) REFERENCES `paralelos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_promociones_ibfk_2` FOREIGN KEY (`idpromocion`) REFERENCES `promocions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_promociones_ibfk_3` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
