<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware'=>['guest']],function(){
    Route::get('/','Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
});

Route::group(['middleware'=>['auth']],function(){
    
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/dashboard', 'DashboardController');
    //Notificaciones
    Route::post('/notification/get', 'NotificationController@get');
    
    Route::get('/main', function () {
        return view('contenido/contenido');
    })->name('main');

    Route::group(['middleware' => ['Almacenero']], function () {
        

        

    });

    Route::group(['middleware' => ['Vendedor']], function () {

        Route::get('/promocion', 'PromocionController@index');
        Route::post('/promocion/registrar', 'PromocionController@store');
        Route::put('/promocion/actualizar', 'PromocionController@update');
        Route::put('/promocion/desactivar', 'PromocionController@desactivar');
        Route::put('/promocion/activar', 'PromocionController@activar');
        Route::get('/promocion/selectPromocion', 'PromocionController@selectPromocion');

        Route::get('/paralelo', 'ParaleloController@index');
        Route::post('/paralelo/registrar', 'ParaleloController@store');
        Route::put('/paralelo/actualizar', 'ParaleloController@update');
        Route::put('/paralelo/desactivar', 'ParaleloController@desactivar');
        Route::put('/paralelo/activar', 'ParaleloController@activar');
        Route::get('/paralelo/selectParalelo', 'ParaleloController@selectParalelo');
        
        Route::get('/instituto', 'institutoController@index');
        Route::post('/instituto/registrar', 'institutoController@store');
        Route::put('/instituto/actualizar', 'institutoController@update');
        Route::put('/instituto/desactivar', 'institutoController@desactivar');
        Route::put('/instituto/activar', 'institutoController@activar');
        Route::get('/instituto/selectInstituto', 'institutoController@selectInstituto');
       
        

        Route::get('/carrera', 'CarreraController@index');
        Route::post('/carrera/registrar', 'CarreraController@store');
        Route::put('/carrera/actualizar', 'CarreraController@update');
        Route::put('/carrera/desactivar', 'CarreraController@desactivar');
        Route::put('/carrera/activar', 'CarreraController@activar');
        Route::get('/carrera/selectCategoria', 'CarreraController@selectCategoria');
        Route::get('/reporteCarrera', 'CarreraController@carreraReporte');
        Route::get('/carrera/selectBuscarCarrera', 'CarreraController@selectbuscarCarrera');

        Route::get('/estudiantePromocion', 'PersonalPromocionController@index');
        Route::post('/estudiantePromocion/registrar', 'PersonalPromocionController@store');
        Route::put('/estudiantePromocion/actualizar', 'PersonalPromocionController@update');
        Route::put('/estudiantePromocion/desactivar', 'PersonalPromocionController@desactivar');
        Route::put('/estudiantePromocion/activar', 'PersonalPromocionController@activar');
        Route::get('/estudiantePromocion/selectCategoria', 'PersonalPromocionController@selectCategoria');
        Route::get('/reporteEstudiantePromocion', 'PersonalPromocionController@carreraReporte');

        Route::post('/datosPersonal', 'PersonalController@index');

        Route::post('/datosEstudioPersonal', 'PersonalEstudioController@index');
        Route::post('/datosEstudioPersonal/registrar', 'PersonalEstudioController@store');
        Route::put('/datosEstudioPersonal/actualizar', 'PersonalEstudioController@update');

        Route::get('/reporteUniversidad', 'ReportesController@univerReport');
        Route::get('/reporteEstudiante', 'ReportesController@estuReport');
        
    });

    Route::group(['middleware' => ['Administrador']], function () {

        Route::get('/promocion', 'PromocionController@index');
        Route::post('/promocion/registrar', 'PromocionController@store');
        Route::put('/promocion/actualizar', 'PromocionController@update');
        Route::put('/promocion/desactivar', 'PromocionController@desactivar');
        Route::put('/promocion/activar', 'PromocionController@activar');
        Route::get('/promocion/selectPromocion', 'PromocionController@selectPromocion');

        Route::get('/paralelo', 'ParaleloController@index');
        Route::post('/paralelo/registrar', 'ParaleloController@store');
        Route::put('/paralelo/actualizar', 'ParaleloController@update');
        Route::put('/paralelo/desactivar', 'ParaleloController@desactivar');
        Route::put('/paralelo/activar', 'ParaleloController@activar');
        Route::get('/paralelo/selectParalelo', 'ParaleloController@selectParalelo');
        
        Route::get('/instituto', 'institutoController@index');
        Route::post('/instituto/registrar', 'institutoController@store');
        Route::put('/instituto/actualizar', 'institutoController@update');
        Route::put('/instituto/desactivar', 'institutoController@desactivar');
        Route::put('/instituto/activar', 'institutoController@activar');
        Route::get('/instituto/selectInstituto', 'institutoController@selectInstituto');
       
        

        Route::get('/carrera', 'CarreraController@index');
        Route::post('/carrera/registrar', 'CarreraController@store');
        Route::put('/carrera/actualizar', 'CarreraController@update');
        Route::put('/carrera/desactivar', 'CarreraController@desactivar');
        Route::put('/carrera/activar', 'CarreraController@activar');
        Route::get('/carrera/selectCategoria', 'CarreraController@selectCategoria');
        Route::get('/reporteCarrera', 'CarreraController@carreraReporte');
        Route::get('/carrera/selectBuscarCarrera', 'CarreraController@selectbuscarCarrera');

        Route::get('/estudiantePromocion', 'PersonalPromocionController@index');
        Route::post('/estudiantePromocion/registrar', 'PersonalPromocionController@store');
        Route::put('/estudiantePromocion/actualizar', 'PersonalPromocionController@update');
        Route::put('/estudiantePromocion/desactivar', 'PersonalPromocionController@desactivar');
        Route::put('/estudiantePromocion/activar', 'PersonalPromocionController@activar');
        Route::get('/estudiantePromocion/selectCategoria', 'PersonalPromocionController@selectCategoria');
        Route::get('/reporteEstudiantePromocion', 'PersonalPromocionController@carreraReporte');

        Route::post('/datosPersonal', 'PersonalController@index');

        Route::post('/datosEstudioPersonal', 'PersonalEstudioController@index');
        Route::post('/datosEstudioPersonal/registrar', 'PersonalEstudioController@store');
        Route::put('/datosEstudioPersonal/actualizar', 'PersonalEstudioController@update');
       
        Route::get('/rol', 'RolController@index');
        Route::get('/rol/selectRol', 'RolController@selectRol');
        
        Route::get('/user', 'UserController@index');
        Route::post('/user/registrar', 'UserController@store');
        Route::put('/user/actualizar', 'UserController@update');
        Route::put('/user/desactivar', 'UserController@desactivar');
        Route::put('/user/activar', 'UserController@activar');
        

        Route::get('/reporteUniversidad', 'ReportesController@univerReport');
        Route::get('/reporteEstudiante', 'ReportesController@estuReport');
    });

});


