<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalEstudio extends Model
{
    protected $table = 'personal_estudios';
    protected $fillable = ['id','idpersonal','iduniversidad','idcarrera','fecha_ingreso','nivel','estado','observacion'];
}
