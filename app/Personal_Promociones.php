<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal_Promociones extends Model
{
    protected $table = 'personal_promociones';
    protected $fillable = ['id','idpersona','idpromocion','idparalelo','estado','observacion'];
}
