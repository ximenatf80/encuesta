<?php

namespace App\Http\Controllers;

use App\Personal_Promociones;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Exception;

class PersonalPromocionController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $personasPromocion = Personal_Promociones::join('personas','personal_promociones.idpersona','=','personas.id')
            ->join('paralelos','personal_promociones.idparalelo','paralelos.id')
            ->join('promocions','personal_promociones.idpromocion','promocions.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento',
            'personas.email','personas.telefono','personas.direccion','personas.estado','personal_promociones.id as estudios_id',
            'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
            'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
            ->where('personas.estado',1)->orderBy('personal_promociones.id', 'desc')->paginate(10);
            
        }
        else{
            $personasPromocion = Personal_Promociones::join('personas','personal_promociones.idpersona','=','personas.id')
            ->join('paralelos','personal_promociones.idparalelo','paralelos.id')
            ->join('promocions','personal_promociones.idpromocion','promocions.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento',
            'personas.email','personas.telefono','personas.direccion','personas.estado','personal_promociones.id as estudios_id',
            'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
            'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
            ->where('personas.estado',1)->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }
        

        return [
            'pagination' => [
                'total'        => $personasPromocion->total(),
                'current_page' => $personasPromocion->currentPage(),
                'per_page'     => $personasPromocion->perPage(),
                'last_page'    => $personasPromocion->lastPage(),
                'from'         => $personasPromocion->firstItem(),
                'to'           => $personasPromocion->lastItem(),
            ],
            'personasPrommocion' => $personasPromocion
        ];
    }  

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->estado_civil = $request->estado_civil;
            $persona->sexo = $request->sexo;
            $persona->fecha_nac = $request->fecha_nac;
            $persona->tipo_documento = 'CI';
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->estado = '1';
            $persona->observacion = "NINGUNA";
            $persona->save();

            $personal_promocion = new Personal_Promociones();
            $personal_promocion->idpersona = $persona->id;
            $personal_promocion->idpromocion = $request->idpromocion;
            $personal_promocion->idparalelo = $request->idparalelo;
            $personal_promocion->estado = '1';   
            $personal_promocion->observacion = 'NINGUNA';            
            $personal_promocion->save();

            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
       // return response()->json($request);
        $persona = Persona::findOrFail($request->id);
        $persona->nombre = $request->nombre;
        $persona->paterno = $request->paterno;
        $persona->materno = $request->materno;
        $persona->estado_civil = $request->estado_civil;
        $persona->sexo = $request->sexo;
        $persona->fecha_nac = $request->fecha_nac;
        $persona->tipo_documento = 'CI';
        $persona->num_documento = $request->num_documento;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;
        $persona->estado = '1';
        $persona->observacion = "NINGUNA";
        $persona->save();

        $personal_promocion = Personal_Promociones::findOrFail($persona->id);
        $personal_promocion->idpromocion = $request->idpromocion;
        $personal_promocion->idparalelo = $request->idparalelo;
        $personal_promocion->estado = '1';   
        $personal_promocion->observacion = 'NINGUNA';            
        $personal_promocion->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Personal_Promociones::findOrFail($request->id);
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Personal_Promociones::findOrFail($request->id);
        $user->estado = '1';
        $user->save();
    }

    public function carreraReporte(Request $request)
    {
        //$per_codigo = $request->per_codigo;

        $carreras = DB::table('carreras')
        ->join('institutos', 'carreras.iduniversidad','institutos.id')
        ->select('carreras.id',
                'carreras.nombre',
                'carreras.nivel',
                'carreras.observacion',
                'institutos.nombre as instituto',
                )
        ->orderBy('id','desc')
        ->get();

        /*$qr = QrCode::format('png')->size(100)->margin(0)->merge('../public/assets/img/qr.png')->generate("NRO: $numDoc\n NOMBRE: $datosQr\n FECHA: $date\n ANTECEDENTE $casoQr");
        $codigo = $qr;*/
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha_emision = date('d')." de ".$meses[date('n')-1]." del ".date('Y');
        $qr = QrCode::encoding('UTF-8')->size(100)->generate("FECHA: $fecha_emision");
        $codigo = $qr;

        $pdf = PDF::loadView('reportes.repcarreras',['carreras'=>$carreras,
                                                        'qr'=>$codigo])
        ->setPaper('letter', 'portrait');                                               
        
        return $pdf->stream('reporte.pdf');

    }
}
