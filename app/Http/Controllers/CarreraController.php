<?php

namespace App\Http\Controllers;

use App\Carrera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CarreraController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $carreras = Carrera::join('institutos','carreras.iduniversidad','=','institutos.id')
            ->select('carreras.id','carreras.iduniversidad','carreras.nombre','carreras.nivel','carreras.observacion','carreras.estado'
            ,'institutos.id as instituto_id','institutos.nombre as instituto')
            ->orderBy('carreras.id', 'desc')->paginate(10);
        }
        else{
            $carreras = Carrera::join('institutos','carreras.iduniversidad','=','institutos.id')
            ->select('carreras.id','carreras.iduniversidad','carreras.nombre','carreras.nivel','carreras.observacion','carreras.estado'
            ,'institutos.id as instituto_id','institutos.nombre as instituto')
            ->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }
        

        return [
            'pagination' => [
                'total'        => $carreras->total(),
                'current_page' => $carreras->currentPage(),
                'per_page'     => $carreras->perPage(),
                'last_page'    => $carreras->lastPage(),
                'from'         => $carreras->firstItem(),
                'to'           => $carreras->lastItem(),
            ],
            'carreras' => $carreras
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $carrera = new Carrera();
        $carrera->iduniversidad = $request->iduniversidad;
        $carrera->nombre = $request->nombre;
        $carrera->nivel = $request->nivel;
        $carrera->estado = '1';
        $carrera->observacion = $request->observacion;
        $carrera->save();       
    }

    public function update(Request $request){

        if (!$request->ajax()) return redirect('/');
        //return response()->json($request->id);
        $carrera = Carrera::findOrFail($request->id);
        $carrera->iduniversidad = $request->iduniversidad;
        $carrera->nombre = $request->nombre;
        $carrera->nivel = $request->nivel;
        $carrera->estado = '1';
        $carrera->observacion = $request->observacion;
        $carrera->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Carrera::findOrFail($request->id);
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Carrera::findOrFail($request->id);
        $user->estado = '1';
        $user->save();
    }

    public function carreraReporte(Request $request)
    {
        //$per_codigo = $request->per_codigo;

        $carreras = DB::table('carreras')
        ->join('institutos', 'carreras.iduniversidad','institutos.id')
        ->select('carreras.id',
                'carreras.nombre',
                'carreras.nivel',
                'carreras.observacion',
                'institutos.nombre as instituto',
                )
        ->orderBy('id','desc')
        ->get();

        /*$qr = QrCode::format('png')->size(100)->margin(0)->merge('../public/assets/img/qr.png')->generate("NRO: $numDoc\n NOMBRE: $datosQr\n FECHA: $date\n ANTECEDENTE $casoQr");
        $codigo = $qr;*/
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha_emision = date('d')." de ".$meses[date('n')-1]." del ".date('Y');
        $qr = QrCode::encoding('UTF-8')->size(100)->generate("FECHA: $fecha_emision");
        $codigo = $qr;

        $pdf = PDF::loadView('reportes.repcarreras',['carreras'=>$carreras,
                                                        'qr'=>$codigo])
        ->setPaper('letter', 'portrait');                                               
        
        return $pdf->stream('reporte.pdf');

    }
    public function selectbuscarCarrera(Request $request){
        $buscar = $request->buscar;
        
        if (!$request->ajax()) return redirect('/');
        $carreras = Carrera::where('iduniversidad','=',$buscar)
        ->where('estado','=','1')
        ->select('id','nombre')->orderBy('id', 'asc')->get();
        return ['carreras' => $carreras];
    } 

    public function selectCarrera(Request $request){
        if (!$request->ajax()) return redirect('/');
        $carreras = Carrera::where('estado','=','1')
        ->select('id','nombre')->orderBy('nombre', 'asc')->get();
        return ['carreras' => $carreras];
    }

}
