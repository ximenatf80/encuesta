<?php

namespace App\Http\Controllers;

use App\Paralelo;
use Illuminate\Http\Request;

class ParaleloController extends Controller
{
    public function index(Request $request){
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $paralelos = Paralelo::orderBy('id', 'desc')->paginate(5);
        }
        else{
            $paralelos = Paralelo::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(5);
        }
        

        return [
            'pagination' => [
                'total'        => $paralelos->total(),
                'current_page' => $paralelos->currentPage(),
                'per_page'     => $paralelos->perPage(),
                'last_page'    => $paralelos->lastPage(),
                'from'         => $paralelos->firstItem(),
                'to'           => $paralelos->lastItem(),
            ],
            'paralelos' => $paralelos
        ];
    }

    public function selectParalelo(Request $request){
        if (!$request->ajax()) return redirect('/');
        $paralelos = Paralelo::where('condicion','=','1')
        ->select('id','detalle')->orderBy('detalle', 'asc')->get();
        return ['paralelos' => $paralelos];
    }

    public function store(Request $request){
        if (!$request->ajax()) return redirect('/');
        $paralelo = new Paralelo();
        $paralelo->detalle = $request->detalle;
        $paralelo->condicion = '1';
        $paralelo->save();
    }

    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
        $paralelo = Paralelo::findOrFail($request->id);
        $paralelo->detalle = $request->detalle;
        $paralelo->condicion = '1';
        $paralelo->save();
    }

    public function desactivar(Request $request){
        if (!$request->ajax()) return redirect('/');
        $paralelo = Paralelo::findOrFail($request->id);
        $paralelo->condicion = '0';
        $paralelo->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $paralelo = Paralelo::findOrFail($request->id);
        $paralelo->condicion = '1';
        $paralelo->save();
    }
}
