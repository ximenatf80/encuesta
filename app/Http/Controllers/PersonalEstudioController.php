<?php

namespace App\Http\Controllers;

use App\PersonalEstudio;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PersonalEstudioController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $id = $request->id;
        $personal_datos = DB::table('personas') 
        ->select('personas.id',
                'personas.paterno',
                'personas.materno',
                'personas.nombre',
                'personas.telefono',
                'personas.num_documento',
                'personas.email'
                )
        ->where('personas.id','=',$id)
        ->where('personas.estado',1)
        ->first();

        $personasEstudio = PersonalEstudio::join('personas','personal_estudios.idpersonal','=','personas.id')
        ->join('institutos','personal_estudios.iduniversidad','institutos.id')
        ->join('carreras','personal_estudios.idcarrera','carreras.id')
        ->select('personas.id','personas.estado','personal_estudios.id as estudios_id','personal_estudios.fecha_ingreso',
        'personal_estudios.idpersonal','personal_estudios.iduniversidad','personal_estudios.idcarrera',
        'personal_estudios.estado','personal_estudios.nivel','institutos.nombre','carreras.nombre as carrera')
        ->where('personas.estado',1)->where('personal_estudios.idpersonal','=',$id)->orderBy('id', 'desc')->get();

        return response()->json(['personal_datos' => $personal_datos, 'estudiosPersonal' => $personasEstudio]);
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        
        $personalEstudio = new PersonalEstudio();
       // return response()->json($request);
        $personalEstudio->idpersonal = $request->idpersonal;
        $personalEstudio->iduniversidad = $request->iduniversidad;
        $personalEstudio->idcarrera = $request->idcarrera;
        $personalEstudio->fecha_ingreso = $request->fecha_ingreso;
        $personalEstudio->nivel = $request->nivel;
        $personalEstudio->estado = '1';   
        $personalEstudio->observacion = 'NINGUNA';            
        $personalEstudio->save();
    }

    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
      //  return response()->json($request->id);
        
        $personalEstudio = PersonalEstudio::findOrFail($request->id);
        $personalEstudio->idpromocion = $request->idpromocion;
        $personalEstudio->idparalelo = $request->idparalelo;
        $personalEstudio->estado = '1';   
        $personalEstudio->observacion = 'NINGUNA';            
        $personalEstudio->save();
    }
}
