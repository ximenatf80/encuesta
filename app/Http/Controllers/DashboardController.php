<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function __invoke(Request $request)
    {
        if(!$request->ajax()) return view('/');

        $cantidad   =   DB::table('personal_estudios as pe')
                            ->join('institutos as i', 'pe.iduniversidad','i.id')
                            ->select(DB::raw('COUNT(pe.iduniversidad) as total'),'pe.iduniversidad','i.sigla')
                            ->where('pe.estado',1)
                            ->groupBy('i.sigla','pe.iduniversidad')
                            // ->groupBy(DB::raw('personal_estudios.iduniversidad'))
                            // ->groupBy(DB::raw('institutos.sigla'))
                            ->take(5)
                            ->get();

        // return ['cantidades'=>$cantidad];
        return response()->json($cantidad);  
    }  
   
    public function obtenerUniversidades(Request $request)
    {
        if(!$request->ajax()) return view('/');

        $cantidad   =   DB::table('personal_estudios as pe')
                            ->select(DB::raw('COUNT(pe.iduniversidad) as total'),'pe.iduniversidad')
                            ->where('pe.estado',1)
                            ->groupBy('pe.iduniversidad')
                            ->take(5)
                            ->get();
                            
        return response()->json($cantidad);  
    }
}
