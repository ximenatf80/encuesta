<?php

namespace App\Http\Controllers;

use App\Instituto;
use Illuminate\Http\Request;

class InstitutoController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $institutos = Instituto::orderBy('id', 'desc')->paginate(5);
        }
        else{
            $institutos = Instituto::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(5);
        }
        

        return [
            'pagination' => [
                'total'        => $institutos->total(),
                'current_page' => $institutos->currentPage(),
                'per_page'     => $institutos->perPage(),
                'last_page'    => $institutos->lastPage(),
                'from'         => $institutos->firstItem(),
                'to'           => $institutos->lastItem(),
            ],
            'institutos' => $institutos
        ];
    }

    public function selectInstituto(Request $request){
        if (!$request->ajax()) return redirect('/');
        $institutos = Instituto::where('estado','=','1')
        ->select('id','nombre')->orderBy('nombre', 'asc')->get();
        return ['institutos' => $institutos];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $instituto = new Instituto();
        $instituto->nombre = $request->nombre;
        $instituto->sigla = $request->sigla;
        $instituto->tipo = $request->tipo;
        $instituto->estado = '1';
        $instituto->observacion = $request->observacion;
        $instituto->save();       
    }

    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
        $instituto = Instituto::findOrFail($request->id);
        $instituto->nombre = $request->nombre;
        $instituto->sigla = $request->sigla;
        $instituto->tipo = $request->tipo;
        $instituto->estado = '1';
        $instituto->observacion = $request->observacion;
        $instituto->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Instituto::findOrFail($request->id);
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Instituto::findOrFail($request->id);
        $user->estado = '1';
        $user->save();
    }
}
