<?php

namespace App\Http\Controllers;

use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalController extends Controller
{
    public function  index(Request $request)
    {
        $id = $request->id;
        $personal_datos = DB::table('personas') 
        ->select('personas.id',
                'personas.paterno',
                'personas.materno',
                'personas.nombre',
                'personas.telefono',
                'personas.num_documento',
                'personas.email'
                )
        ->where('personas.id','=',$id)
        ->where('personas.estado',1)
        ->first();
        return response()->json(['personal_datos' => $personal_datos]);
       // return response()->json($personal_datos);

    }
}
