<?php

namespace App\Http\Controllers;
use App\Promocion;
use Illuminate\Http\Request;

class PromocionController extends Controller
{
    public function index(Request $request){
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $promociones = Promocion::orderBy('anio', 'desc')->paginate(5);
        }
        else{
            $promociones = Promocion::where($criterio, 'like', '%'. $buscar . '%')->orderBy('anio', 'desc')->paginate(5);
        }
        

        return [
            'pagination' => [
                'total'        => $promociones->total(),
                'current_page' => $promociones->currentPage(),
                'per_page'     => $promociones->perPage(),
                'last_page'    => $promociones->lastPage(),
                'from'         => $promociones->firstItem(),
                'to'           => $promociones->lastItem(),
            ],
            'promociones' => $promociones
        ];
    }

    public function selectPromocion(Request $request){
        if (!$request->ajax()) return redirect('/');
        $promociones = Promocion::where('condicion','=','1')
        ->select('id','anio')->orderBy('anio', 'asc')->get();
        return ['promociones' => $promociones];
    }

    public function store(Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = new Promocion();
        $promocion->anio = $request->anio;
        $promocion->descripcion_promo = $request->descripcion_promo;
        $promocion->total = $request->total;
        $promocion->condicion = '1';
        $promocion->save();
    }

    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = Promocion::findOrFail($request->id);
        $promocion->anio = $request->anio;
        $promocion->descripcion_promo = $request->descripcion_promo;
        $promocion->total = $request->total;
        $promocion->condicion = '1';
        $promocion->save();
    }

    public function desactivar(Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = Promocion::findOrFail($request->id);
        $promocion->condicion = '0';
        $promocion->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $promocion = Promocion::findOrFail($request->id);
        $promocion->condicion = '1';
        $promocion->save();
    }
}
