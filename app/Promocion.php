<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocion extends Model
{
    protected $fillable = ['anio','descripcion_promo','total','condicion'];
}
